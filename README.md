# Hello, Universe

Say hello to universe.

## Install

```sh
$ composer require siarie/hello-universe
```

## Usage

```php
<?php

require_once __DIR__ . "your vendor path";

use Siarie\HelloUniverse;

$hello = HelloUniverse::sayHello();

echo $hello;

```

# Licence
See the [LICENSE](./LICENSE) file.
