#!/bin/env php

<?php


require_once __DIR__ . "/vendor/autoload.php";


use Siarie\HelloUniverse;


printf("%s\n", HelloUniverse::sayHello());
