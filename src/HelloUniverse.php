<?php

namespace Siarie;


final class HelloUniverse
{
	public static function sayHello()
	{
		return "Hello, Universe.";
	}
}
